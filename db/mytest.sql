-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 13, 2017 at 06:06 AM
-- Server version: 5.7.14
-- PHP Version: 7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mytest`
--

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `access_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `expires` timestamp NOT NULL,
  `scope` varchar(4000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`access_token`, `client_id`, `user_id`, `expires`, `scope`) VALUES
('baf78c7e46c3be29579ff39291c0540691b97548', 'testclient', NULL, '2017-10-12 05:20:05', NULL),
('544d4deb6ea5d9f53e8baed69c48c8bd190d68b6', 'testclient', NULL, '2017-10-12 05:20:11', NULL),
('2c416421d08f6d860f3e1ba2fdf170f0c0e23d69', 'testclient', NULL, '2017-10-12 05:20:58', NULL),
('6c96733a53a0c9176aebc81514b0262850e10dc2', 'testclient', NULL, '2017-10-12 05:21:10', NULL),
('5318080df89bc1efebe07f432ec4122c8c8ebab7', 'testclient', NULL, '2017-10-12 05:25:10', NULL),
('d921da4126e58dad2a51a954ba161b316672dc12', 'testclient', NULL, '2017-10-12 05:32:36', NULL),
('4bc6a8d72a2cab91d1869da3848d72a807d40baa', 'testclient', NULL, '2017-10-12 05:36:19', NULL),
('3c232183cbde85da4fa0e4af4cbc8706fb595f29', 'testclient', NULL, '2017-10-12 05:36:39', NULL),
('adb9a0e5d40501342351bcd90d11eddfd472f4cc', 'testclient', NULL, '2017-10-12 05:36:59', NULL),
('74c86fbf730aca9f490235443a68414f254aa33c', 'testclient', NULL, '2017-10-12 05:37:10', NULL),
('37dfd6573f8e363658bd069caee2db9b0f9936bc', 'testclient', NULL, '2017-10-12 05:37:32', NULL),
('f1bcb322a5e69df26d007a0aa163b03062e33ea9', 'testclient', NULL, '2017-10-12 05:37:44', NULL),
('04c85e3907172889aa2631c6bb02135ed469a8f0', 'testclient', NULL, '2017-10-12 05:39:45', NULL),
('71af9e2522d0d7dd849797626b0e910f3023ef2a', 'testclient', NULL, '2017-10-12 05:39:52', NULL),
('0d42f666600c468a4284240aa24ec54d065bc861', 'testclient', NULL, '2017-10-12 05:39:59', NULL),
('754020c460c3606a30bffdb7eded53b874fc3753', 'testclient', NULL, '2017-10-12 05:40:15', NULL),
('7d55cdaeb20c6fce1e3e5b06efee0d1956fc1437', 'testclient', NULL, '2017-10-12 05:41:28', NULL),
('ec1001cbac1560d769fa41aef408a46e7b762bc9', 'testclient', NULL, '2017-10-12 05:41:35', NULL),
('62cd426dd41f4e1379ac77716fabaf12b930d67b', 'testclient', NULL, '2017-10-12 05:41:50', NULL),
('553cd6a9edc5c6a5b5c68ec58a3f7039fdf73d40', 'testclient', NULL, '2017-10-12 05:42:15', NULL),
('b09450592f24738020503b1e4621144431bec777', 'testclient', NULL, '2017-10-12 05:42:23', NULL),
('0a4b6620478cc8e5f93d5fa80d845fb21b4f19d1', 'testclient', NULL, '2017-10-12 05:42:29', NULL),
('fff725b4b0fc6829b244d1c8e4094e52085e7b7b', 'testclient', NULL, '2017-10-12 05:42:36', NULL),
('753e80d4d3644c47967d6028c08221eba8c3da25', 'testclient', NULL, '2017-10-12 05:43:01', NULL),
('c8e950e4af9b0bb965c2ddf9a77543f8d18a3e15', 'testclient', NULL, '2017-10-12 05:43:18', NULL),
('054f47d5156bb866759345808e26af81f3f56d53', 'testclient', NULL, '2017-10-12 05:43:26', NULL),
('35bee29fd996a42e628f1e6dc2f06075d270a2f9', 'testclient', NULL, '2017-10-12 05:43:53', NULL),
('6727e42f1f1a1098da7bd3abef5220d17bb039b0', 'testclient', NULL, '2017-10-12 05:44:05', NULL),
('147e2af9fa86bb5e44891c88730fe75c9bd5a86e', 'testclient', NULL, '2017-10-12 05:44:39', NULL),
('1fad3a2d349b505302bac6a2e8bdaee999254374', 'testclient', NULL, '2017-10-12 05:44:48', NULL),
('ca776c7c2b85e34a84dc4a189bb35c9e3b819200', 'testclient', NULL, '2017-10-12 05:45:08', NULL),
('e3880a6ddb48558f531170f50be9d05401a09d8b', 'testclient', NULL, '2017-10-12 05:45:16', NULL),
('8d43424dbc061cc21b384237cccba323eb76eb2f', 'testclient', NULL, '2017-10-12 05:45:25', NULL),
('359a2d48bf9640059f7fac3f77f9a1c42cd565d9', 'testclient', NULL, '2017-10-12 05:45:41', NULL),
('21ce4f3e5286d4804303999c7302220c0d81ae0e', 'testclient', NULL, '2017-10-12 05:45:50', NULL),
('8bafad2ed412b3feb043ad47de76e43bda3e818c', 'testclient', NULL, '2017-10-12 05:45:57', NULL),
('bdcea71ad2824a9055381cc012ff2c275d271b19', 'testclient', NULL, '2017-10-12 05:46:48', NULL),
('5e556a51dbdd4b6ef7e92b8a4e14108d8f8bbe17', 'testclient', NULL, '2017-10-12 05:47:14', NULL),
('6e49368c396a95578a390c1df2c8c986d12333b7', 'testclient', NULL, '2017-10-12 05:47:25', NULL),
('f7a33f6464a3494449c355f7b26069ab1c9a4b41', 'testclient', NULL, '2017-10-12 05:47:40', NULL),
('0d4c9f1161a9defa19cc8e747c924fda9f2fe13c', 'testclient', NULL, '2017-10-12 05:47:52', NULL),
('c92bcdaad413067b3032dd9f91106689dcb42607', 'testclient', NULL, '2017-10-12 05:47:58', NULL),
('abd18453de534a84467a8d738842f5be707f7c91', 'rafay@admin.com', NULL, '2017-10-12 05:54:30', NULL),
('6e8256b17145dd60b1edbd145ba845c2df2be904', 'rafay@admin.com', NULL, '2017-10-12 05:55:04', NULL),
('f5437c0c181607352fbd4b7145840043f4567c37', 'rafay@admin.com', NULL, '2017-10-12 06:00:59', NULL),
('075b7377c39b94ce2c979748d522485659297a6e', 'rafay@admin.com', NULL, '2017-10-12 06:01:06', NULL),
('78400934126feec28244e8be18533f132ddfc865', 'rafay@admin.com', NULL, '2017-10-12 06:01:47', NULL),
('02c89325d5a67d7c10669165ce81ff100aec29ea', 'rafay@admin.com', NULL, '2017-10-12 06:02:32', NULL),
('68760cc0dbde0e8894037c62fe830656eb0745df', 'rafay@admin.com', NULL, '2017-10-12 06:06:31', NULL),
('7d812bd5e758cb01f327ba700341836b0e473d70', 'rafay@admin.com', NULL, '2017-10-12 06:07:53', NULL),
('bd44262c3121d4531e2e73e547daf58d0c3d7e8b', 'rafay@admin.com', NULL, '2017-10-12 06:08:29', NULL),
('bc351e4e43908586815dc51fe205c78a6d03b245', 'rafay@admin.com', NULL, '2017-10-12 06:11:53', NULL),
('0c00978fd51b1f6cfdcc3f4b4ada05f56d179edd', 'rafay@admin.com', NULL, '2017-10-12 06:12:49', NULL),
('4e553b8cf7b6ed7fb6206786ac7d9d5303d2a1a0', 'rafay@admin.com', NULL, '2017-10-12 06:18:20', NULL),
('b04ea5964ed5bed2d296ada1a3f9f2e9d7e49e9e', 'rafay@user.com', NULL, '2017-10-12 06:23:29', NULL),
('fe303a50c487bbb06ef95b2b3230e1ecd86939a4', 'rafay@admin.com', NULL, '2017-10-12 06:23:33', NULL),
('d1d364268ca48d60523836c3382a4f30e85744dc', 'rafay@admin.com', NULL, '2017-10-12 07:49:27', NULL),
('9761ec67759b0793a5dafe66d5feeb074d9d0903', 'rafay@admin.com', NULL, '2017-10-12 07:54:23', NULL),
('50d3fe70bb977d1540e78d165db959863b3360e9', 'rafay@admin.com', NULL, '2017-10-12 08:00:25', NULL),
('d9878660353a8c28a649ac5375e16209a1917b3d', 'rafay@admin.com', NULL, '2017-10-12 08:03:45', NULL),
('275389cac651627a5ee6e3c4faf41072dc652be8', 'rafay@admin.com', NULL, '2017-10-12 08:08:56', NULL),
('bab82e69b30228947fccd634fa7e6071fee38cdc', 'rafay@user.com', NULL, '2017-10-12 08:28:19', NULL),
('47c24e076103648da1eb4aa5ff9ea4d4ea835528', 'rafay@admin.com', NULL, '2017-10-12 08:28:57', NULL),
('98e56c2022480feddf6f55b75c91606e036f6dd4', 'rafay@admin.com', NULL, '2017-10-12 08:31:55', NULL),
('e5f5ea8e12a3efc88023c6456041eaaa728c4471', 'rafay@user.com', NULL, '2017-10-12 08:35:41', NULL),
('7585f486061be496ffc31a7a12d66c28e5a917fa', 'rafay@user.com', NULL, '2017-10-12 08:36:04', NULL),
('53e3fe0f3d24ff72bfa296026bed171fc67ecbdd', 'rafay@admin.com', NULL, '2017-10-12 08:36:07', NULL),
('10d02fed9d7b35ba879e95a835339893e2a01be7', 'rafay@admin.com', NULL, '2017-10-12 09:08:49', NULL),
('bd12ee724db5d5c178cbec34f823b897ce921982', 'rafay@admin.com', NULL, '2017-10-12 09:45:18', NULL),
('851727c38f27d1eef2c7e00863412f44de4aabb8', 'rafay@admin.com', NULL, '2017-10-13 01:06:35', NULL),
('12497eaef6a71956106cec0922b2a8fe8eab2317', 'rafay@admin.com', NULL, '2017-10-13 01:19:52', NULL),
('c0c5442dcf5ea2ff65f7fdbf6c92ad3de9355716', '102@rafay.com', NULL, '2017-10-13 01:32:59', NULL),
('3b21da7358cc4e66a9088cf4f94372babacdb644', '102@rafay.com', NULL, '2017-10-13 01:33:12', NULL),
('dd73c30ad811c1226dcf3af7be8f1122a42cd5f0', 'rafay@admin.com', NULL, '2017-10-13 01:33:18', NULL),
('fecb1265445c3163e80f2d21f6b05c882faf250e', 'test@test.com', NULL, '2017-10-13 01:33:47', NULL),
('ceba199669d63e0d495a09b0c6c6dc2a8a45aa47', 'test@test.com', NULL, '2017-10-13 01:33:59', NULL),
('581a59ce3b9d77a4a75eb929e34f6a21c3e6dc0c', 'rafay@admin.com', NULL, '2017-10-13 01:34:03', NULL),
('6fee8440aa55844efa0d9566c6b1e67589ef6806', 'test@test.com', NULL, '2017-10-13 01:34:18', NULL),
('0bc4b39686980bbaa4e47cac414840393c125496', 'rafay@admin.com', NULL, '2017-10-13 01:42:46', NULL),
('24cb12ba07ae818b7bdb8a78d3c930a599a3d677', 'rafay@admin.com', NULL, '2017-10-13 01:44:20', NULL),
('9cee49d30b6f805923c21f93e23748b82e8f8eaf', 'rafay@admin.com', NULL, '2017-10-13 01:44:28', NULL),
('5d41821f695dfba878101a8fa15563083b3667cf', 'rafay@admin.com', NULL, '2017-10-13 01:46:30', NULL),
('ef641ab08044cf5232b14872c5c4250f0e1146ad', '102@rafay.com', NULL, '2017-10-13 02:00:57', NULL),
('e89fba8bac81c546789529a6f5fab30352d51dc2', '102@rafay.com', NULL, '2017-10-13 02:01:08', NULL),
('4ead0180d97c6b05eea68a561cae95bbab9a8409', '102@rafay.com', NULL, '2017-10-13 02:01:23', NULL),
('e6d77e8d0b0f87209e30e20fd4e791a201e52cb9', 'rafay@admin.com', NULL, '2017-10-13 02:01:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_authorization_codes`
--

CREATE TABLE `oauth_authorization_codes` (
  `authorization_code` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `expires` timestamp NOT NULL,
  `scope` varchar(4000) DEFAULT NULL,
  `id_token` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `client_id` varchar(80) NOT NULL,
  `client_secret` varchar(80) DEFAULT NULL,
  `redirect_uri` varchar(2000) DEFAULT NULL,
  `grant_types` varchar(80) DEFAULT NULL,
  `scope` varchar(4000) DEFAULT NULL,
  `user_id` varchar(80) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`client_id`, `client_secret`, `redirect_uri`, `grant_types`, `scope`, `user_id`) VALUES
('rafay@admin.com', '12345678', 'http://fake/', NULL, NULL, NULL),
('rafay@user.com', '12345678', 'http://fake/', NULL, NULL, NULL),
('rafay@rafay.com', '12345678', 'fake', NULL, NULL, NULL),
('102@rafay.com', '1234', 'fake', NULL, NULL, NULL),
('test@test.com', '123', 'fake', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_jwt`
--

CREATE TABLE `oauth_jwt` (
  `client_id` varchar(80) NOT NULL,
  `subject` varchar(80) DEFAULT NULL,
  `public_key` varchar(2000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `refresh_token` varchar(40) NOT NULL,
  `client_id` varchar(80) NOT NULL,
  `user_id` varchar(80) DEFAULT NULL,
  `expires` timestamp NOT NULL,
  `scope` varchar(4000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_scopes`
--

CREATE TABLE `oauth_scopes` (
  `scope` varchar(80) NOT NULL,
  `is_default` tinyint(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_users`
--

CREATE TABLE `oauth_users` (
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `first_name` varchar(80) DEFAULT NULL,
  `last_name` varchar(80) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `email_verified` tinyint(1) DEFAULT NULL,
  `scope` varchar(4000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '2' COMMENT '1: admin, 2: user',
  `fname` varchar(150) DEFAULT NULL,
  `lname` varchar(150) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `role`, `fname`, `lname`, `email`, `password`) VALUES
(1, 'admin', 1, 'Abdul', 'Rafay', 'rafay@admin.com', '25d55ad283aa400af464c76d713c07ad'),
(2, 'user', 2, 'Shaikh', 'Rafay', 'rafay@user.com', '25d55ad283aa400af464c76d713c07ad'),
(5, '789@rafay.com', 2, '123', '456', '102@rafay.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(6, 'rafay@rafay.com', 1, 'rafay@rafay.com', 'rafay@rafay.com', 'rafay1@rafay.com', '6372fbbccd8ea0de1c450250c09a902a'),
(7, 'test123', 1, 'Test', 'test', 'test@test.com', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`access_token`);

--
-- Indexes for table `oauth_authorization_codes`
--
ALTER TABLE `oauth_authorization_codes`
  ADD PRIMARY KEY (`authorization_code`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`refresh_token`);

--
-- Indexes for table `oauth_scopes`
--
ALTER TABLE `oauth_scopes`
  ADD PRIMARY KEY (`scope`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
