myApp.controller('NavCtrl', ['$scope', '$rootScope', 'common', function ($scope, $rootScope, common) {

    // Get User Detail
    $rootScope.userRole = 2;
    $rootScope.userName = '';
    common.http.get('auth/detail').then(function (response) {
        response = response.data;
        if (response.status) {
            $rootScope.userRole = response.data.role;
            $rootScope.userName = response.data.fname + " " + response.data.lname;
        }
    });
}]);

// Dashboard Controller
myApp.controller('DashboardCtrl', ['$scope', '$rootScope', 'common', function ($scope, $rootScope, common) {

}]);

// User Management Controller
myApp.controller('UserCtrl', ['$scope', '$rootScope', 'common', '$location', '$routeParams', function ($scope, $rootScope, common, $location, $routeParams) {

    // Get User listing
    $scope.userListing = function () {
        common.http.get('user/listing').then(function (response) {
            response = response.data;
            if (response.status) {
                $rootScope.users = response.data;
            }
        });
    }
    $scope.userListing();

    // User Obj
    $scope.user = {
        firstName: '',
        lastName: '',
        userName: '',
        email: '',
        password: '',
        cpassword: '',
        isAdmin: false
    };

    // Save User
    $scope.save = function () {
        $scope.user.isAdmin = ($scope.user.isAdmin) ? 1 : 0;
        common.http.post('save/user', $scope.user).then(function (response) {
            response = response.data;
            if (response.status) {
                common.flashMsg('success', 'User save successfully.');
                $location.path('/user/listing');
            } else {
                common.flashMsg('error', 'Try Unique Email.');
            }
        }, function (error) {
            common.flashMsg('error', 'Server error');

        });
    }

    // Get User Detail By Id
    $scope.userDetail = function () {
        if (typeof $routeParams.id === typeof undefined) {
            return;
        }

        common.http.get('user/detail/' + $routeParams.id).then(function (response) {
            response = response.data;
            if (response.status) {
                $scope.user = {
                    id: response.data.id,
                    firstName: response.data.fname,
                    lastName: response.data.lname,
                    userName: response.data.username,
                    email: response.data.email,
                    oldEmail: response.data.email,
                    password: '',
                    cpassword: '',
                    isAdmin: (response.data.role == 1) ? true : false
                };
            } else {
                common.flashMsg('error', "Umh, We can't find user right now");
                $location.path('/user/listing');
            }
        }, function (error) {
            $scope.user = {
                firstName: '',
                lastName: '',
                userName: '',
                email: '',
                password: '',
                cpassword: '',
                isAdmin: false
            };
        });
    }

    // Check: User Edit Screen
    if (typeof $routeParams.id !== typeof undefined || $routeParams.id !== '') {
        $scope.userDetail();
    }


}]);

// Profile Controller
myApp.controller('ProfileCtrl', ['$scope', '$rootScope', 'common', '$location', '$routeParams', function ($scope, $rootScope, common, $location, $routeParams) {

    // Get User listing
    $scope.profiles = [];
    $scope.profileListing = function () {
        common.http.get('profile/listing').then(function (response) {
            response = response.data;
            if (response.status) {
                $scope.profiles = response.data;
            }
        }, function (error) {
            common.flashMsg('error', 'Server error: Profile listing not found.');

        });
    };

    // Get All Profile Type
    $scope.profileTypes = [];
    $scope.projects = [];
    $scope.getReferenceTable = function (tableName, variable) {
        common.http.post('get/data/table', {tableName: tableName}).then(function (response) {
            response = response.data;
            if (response.status) {
                $scope[variable] = response.data;
            }
        });
    }

    //Profile New Object
    $scope.profile = {
        type: '',
        client_type: '',
        project: '',
        firstname: '',
        lastname: '',
        email: '',
        company: '',
        gender: 'Male',
        photo: 'user-default.png',
        country: '',
        state: '',
        postal_code: '',
        address: '',
        website: '',
        timezone: '',
        work_no: '',
        fax: '',
        cell_no: ''
    };

    // Save Profile
    $scope.save = function () {
        common.http.post('save/profile', $scope.profile).then(function (response) {
            response = response.data;
            if (response.status) {
                common.flashMsg('success', response.message);
                $location.path('/profiles');
            } else {
                common.flashMsg('error', response.message);
            }
        }, function (error) {
            common.flashMsg('error', "Umh, We can't store profile right now");

        });
    }

    // Get Type Name
    $scope.getTypeName = function (id) {
        var find = false;
        var rtValue = '';
        angular.forEach($scope.profileTypes, function (val, key) {
            if (!find) {
                if (parseInt(val['id']) == parseInt(id)) {
                    find = true;
                    rtValue = val['name'];
                }
            }
        });

        return rtValue;
    }

    // Get Profile Detail By Id
    $scope.profileDetail = function () {
        if (typeof $routeParams.id === typeof undefined) {
            return;
        }

        common.http.get('profile/detail/' + $routeParams.id).then(function (response) {
            response = response.data;
            if (response.status) {
                $scope.profile = response.data;
            } else {
                common.flashMsg('error', "Umh, We can't find profile right now");
                $location.path('/profiles');
            }
        }, function (error) {
            $scope.profile = {
                type: '',
                client_type: '',
                project: '',
                firstname: '',
                lastname: '',
                email: '',
                company: '',
                gender: 'Male',
                photo: 'user-default.png',
                country: '',
                state: '',
                postal_code: '',
                address: '',
                website: '',
                timezone: '',
                work_no: '',
                fax: '',
                cell_no: ''
            };
        });
    }

    // Load Campaign Detail By Id
    var url = $location.path();
    if (url.indexOf('profiles') >= 0) {
        $scope.profileListing();
        $scope.getReferenceTable('type', 'profileTypes');
    } else if (url.indexOf('profile/new') >= 0 || url.indexOf('edit/profile') >= 0) {
        $scope.getReferenceTable('type', 'profileTypes');
        $scope.getReferenceTable('projects', 'projects');
        $scope.profileDetail();
    }
}]);

// Project Controller
myApp.controller('ProjectCtrl', ['$scope', '$rootScope', 'common', '$location', '$routeParams', function ($scope, $rootScope, common, $location, $routeParams) {

    // Get Projects listing
    $scope.projects = [];
    $scope.projectsListing = function () {
        common.http.get('project/listing').then(function (response) {
            response = response.data;
            if (response.status) {
                $scope.projects = response.data;
            }
        }, function (error) {
            common.flashMsg('error', 'Server error: Project listing not found.');

        });
    }


    // Get All Profile Type
    $scope.profileTypes = [];
    $scope.projects = [];
    $scope.getReferenceTable = function (tableName, variable) {
        common.http.post('get/data/table', {tableName: tableName}).then(function (response) {
            response = response.data;
            if (response.status) {
                $scope[variable] = response.data;
            }
        });
    }

    //Profile New Object
    $scope.profile = {
        type: '',
        client_type: '',
        project: '',
        firstname: '',
        lastname: '',
        email: '',
        company: '',
        gender: 'Male',
        photo: 'user-default.png',
        country: '',
        state: '',
        postal_code: '',
        address: '',
        website: '',
        timezone: '',
        work_no: '',
        fax: '',
        cell_no: ''
    };

    // Save Profile
    $scope.save = function () {
        common.http.post('save/profile', $scope.profile).then(function (response) {
            response = response.data;
            if (response.status) {
                common.flashMsg('success', response.message);
                $location.path('/profiles');
            } else {
                common.flashMsg('error', response.message);
            }
        }, function (error) {
            common.flashMsg('error', "Umh, We can't store profile right now");

        });
    }

    // Get Type Name
    $scope.getTypeName = function (id) {
        var find = false;
        var rtValue = '';
        angular.forEach($scope.profileTypes, function (val, key) {
            if (!find) {
                if (parseInt(val['id']) == parseInt(id)) {
                    find = true;
                    rtValue = val['name'];
                }
            }
        });

        return rtValue;
    }

    // Get Profile Detail By Id
    $scope.profileDetail = function () {
        if (typeof $routeParams.id === typeof undefined) {
            return;
        }

        common.http.get('profile/detail/' + $routeParams.id).then(function (response) {
            response = response.data;
            if (response.status) {
                $scope.profile = response.data;
            } else {
                common.flashMsg('error', "Umh, We can't find profile right now");
                $location.path('/profiles');
            }
        }, function (error) {
            $scope.profile = {
                type: '',
                client_type: '',
                project: '',
                firstname: '',
                lastname: '',
                email: '',
                company: '',
                gender: 'Male',
                photo: 'user-default.png',
                country: '',
                state: '',
                postal_code: '',
                address: '',
                website: '',
                timezone: '',
                work_no: '',
                fax: '',
                cell_no: ''
            };
        });
    }

    // Load Campaign Detail By Id
    var url = $location.path();
    if (url.indexOf('projects') >= 0) {
        $scope.projectsListing();
    } else if (url.indexOf('profile/new') >= 0 || url.indexOf('edit/profile') >= 0) {
        $scope.getReferenceTable('type', 'profileTypes');
        $scope.getReferenceTable('projects', 'projects');
        $scope.profileDetail();
    }
}]);