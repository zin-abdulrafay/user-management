// Directive to toggle SideMenu
myApp.directive('sidebarCollapse', [function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'C',
        //The link function is responsible for registering DOM listeners as well as updating the DOM.
        link: function (scope, element, attrs) {
            angular.element(element).click(function () {
                angular.element('#sidebar').toggleClass('active');
                angular.element(this).toggleClass('active');
            })
        }
    };
}]);

// Password/Confirm Password Validation
myApp.directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    var v = elem.val() === $(firstPassword).val();
                    ctrl.$setValidity('pwmatch', v);
                });
            });
        }
    }
}]);

// Show Active Menu
myApp.directive('umMenu', [function () {
    return {
        // A = attribute, E = Element, C = Class and M = HTML Comment
        restrict: 'C',
        //The link function is responsible for registering DOM listeners as well as updating the DOM.
        link: function (scope, element, attrs) {
            angular.element(element).find('li').click(function () {
                angular.element(element).find('li').removeClass('active');
                angular.element(this).addClass('active');
            })
        }
    }
}]);