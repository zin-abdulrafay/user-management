<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Reset Password - User Management</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/signin.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <form method="post" action="<?php echo base_url('do/reset/password'); ?>" class="form-signin">
        <h2 class="form-signin-heading">Reset Password</h2>
        <?php if (!empty($this->session->flashdata('resetStatus'))) { ?>
            <p class="alert alert-danger"><?= $this->session->flashdata('resetStatus'); ?></p>
        <?php } ?>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required
               autofocus>
        <div class="checkbox">
        </div>
        <label for="inputPassword" class="sr-only">New Password</label>
        <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password"
               required>
        <div class="checkbox">
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Continue</button>
    </form>

</div> <!-- /container -->
</body>
</html>
