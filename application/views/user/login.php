<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Signin - User Management</title>

    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>assets/css/signin.css" rel="stylesheet">
</head>

<body>

<div class="container">

    <form method="post" action="<?php echo base_url('do/login'); ?>" class="form-signin">
        <input type="hidden" name="grant_type" value="client_credentials">
        <h2 class="form-signin-heading">Please sign in</h2>
        <?php if (!empty($this->session->flashdata('login_status'))) { ?>
            <p class="alert alert-danger"><?= $this->session->flashdata('login_status'); ?></p>
        <?php } ?>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input name="client_id" type="email" id="inputEmail" class="form-control" placeholder="Email address" required
               autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="client_secret" type="password" id="inputPassword" class="form-control" placeholder="Password"
               required>
        <div class="checkbox">
            <label>
                <a href="reset/password">Forgot Password</a>
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </form>

</div> <!-- /container -->
</body>
</html>
