<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Management</title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>bower_components/bootstrap/dist/css/bootstrap.css">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/app.css">
</head>
<body ng-cloak>
<div class="flashMsg"></div>
<div class="wrapper" ng-cloak="">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <div class="sidebar-header">
            <h3><a href="#!/dashboard">User Management</a></h3>
        </div>

        <ul class="list-unstyled components umMenu">
            <!--<p>Dashboard</p><li class="active">
                <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">Home</a>
                <ul class="collapse list-unstyled" id="homeSubmenu">
                    <li><a href="#">Home 1</a></li>
                    <li><a href="#">Home 2</a></li>
                    <li><a href="#">Home 3</a></li>
                </ul>
            </li>-->
            <li class="active">
                <a ng-if="userRole==1" href="#!/dashboard">Dashboard</a>
            </li>
            <li>
                <a ng-if="userRole==1" href="#!/user/listing">Users</a>
            </li>
            <li>
                <a ng-if="userRole==1" href="#!/profiles">Profiles</a>
            </li>
            <li>
                <a ng-if="userRole==1" href="#!/projects">Projects</a>
            </li>
            <li>
                <a ng-if="userRole==1" href="#!/releases">Releases</a>
            </li>
        </ul>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">

        <nav class="navbar navbar-default" ng-controller="NavCtrl">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="navbar-btn sidebarCollapse">
                        <span></span>
                        <span></span>
                        <span></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?php echo base_url('auth/logout'); ?>">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="view page page-fade-in" ng-view ng-cloak=""></div>
    </div>
</div>
<!-- Angular Init Here -->
<script type="text/javascript" src="<?php echo base_url(); ?>bower_components/jquery/dist/jquery.js"></script>
<script src="<?php echo base_url(); ?>bower_components/angular/angular.js"></script>
<script src="<?php echo base_url(); ?>build/js/components.js"></script>


</body>
</html>
