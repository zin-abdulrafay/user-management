<?php

class CIAuth
{
    private $ci;
    public $server;

    public function __construct()
    {
        $this->ci =& get_instance();
        $db = $this->ci->db->database;
        $dsn = 'mysql:dbname=' . $db . ';host=' . $this->ci->db->hostname;
        $username = $this->ci->db->username;
        $password = $this->ci->db->password;

        // error reporting (this is a demo, after all!)
        ini_set('display_errors', 1);
        error_reporting(E_ALL);

        // Autoloading (composer is preferred, but for this example let's just do this)
        OAuth2\Autoloader::register();

        // $dsn is the Data Source Name for your database, for exmaple "mysql:dbname=my_oauth2_db;host=localhost"
        $storage = new OAuth2\Storage\Pdo(array('dsn' => $dsn, 'username' => $username, 'password' => $password));

        // Pass a storage object or array of storage objects to the OAuth2 server class
        $this->server = new OAuth2\Server($storage,array(
            'access_lifetime' => mt_rand(12313131,12313131)
        ));

        // Add the "Client Credentials" grant type (it is the simplest of the grant types)
        $this->server->addGrantType(new OAuth2\GrantType\ClientCredentials($storage));

        // Add the "Authorization Code" grant type (this is where the oauth magic happens)
        $this->server->addGrantType(new OAuth2\GrantType\AuthorizationCode($storage));
    }
}