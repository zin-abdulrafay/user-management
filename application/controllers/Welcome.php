<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if (!$this->session->has_userdata('isLoggedIn') && $this->session->userdata('isLoggedIn') !== 1) {
            redirect('/');
        }
        $this->load->view("index");
    }
}