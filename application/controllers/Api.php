<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    // Get users listing
    public function users($id = '')
    {
        try {
            if (!empty($id)) {
                $this->db->where('id', $id);
            }
            $this->db->from('users');
            $query = $this->db->get();
            if (!empty($id)) {
                $data = $query->row_array();
                if (count($data) <= 0) {
                    echo returnResponse(false, [], 'user listing empty.');
                    die;
                }
            } else {
                $data = $query->result_array();
            }
            //$this->response(returnResponse(true, $data, 'user listing', 'array'));
            echo returnResponse(true, $data, 'user listing');
        } catch (\Exception $e) {
            //$this->response(returnResponse(false, [], 'user listing empty.', 'array'));
            echo returnResponse(false, [], 'user listing empty.');
        }
    }

    // Save User
    public function saveUser()
    {
        try {
            $id = $this->input->post('id');
            $clientData = array(
                'client_id' => $this->input->post('email'),
                'client_secret' => $this->input->post('password'),
                'redirect_uri' => 'fake'
            );
            $data = array(
                'fname' => $this->input->post('firstName'),
                'lname' => $this->input->post('lastName'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'username' => $this->input->post('userName'),
                'role' => ($this->input->post('isAdmin') == 1) ? 1 : 2,
            );
            $clientSave = false;
            if (empty($id)) {
                $clientSave = $this->db->insert('oauth_clients', $clientData);
            } else {
                $this->db->where('client_id', $this->input->post('oldEmail'));
                $clientSave = $this->db->update('oauth_clients', $clientData);
            }

            if ($clientSave) {
                if (empty($id)) {
                    $this->db->insert('users', $data);
                } else {
                    $this->db->where('id', $id);
                    $this->db->update('users', $data);
                }
                echo returnResponse(true, [], 'user save successfully.');
            } else {
                echo returnResponse(false, ['query' => $this->db->last_query()], 'unique email required.');
            }
        } catch (\Exception $e) {
            echo returnResponse(false, [], 'user save failed.');
        }
    }

    // Get Profiles listing
    public function profiles($id = '')
    {
        try {
            if (!empty($id)) {
                $this->db->where('id', $id);
            }
            $this->db->from('profiles');
            $query = $this->db->get();
            if (!empty($id)) {
                $data = $query->row_array();
                if (count($data) <= 0) {
                    echo returnResponse(false, [], 'profile listing empty.');
                    die;
                }
            } else {
                $data = $query->result_array();
            }
            echo returnResponse(true, $data, 'profiles listing');
        } catch (\Exception $e) {
            echo returnResponse(false, [], 'profile listing empty.');
        }
    }

    // Get Generic Table Data
    public function getTableData()
    {
        try {
            $table = $this->input->post('tableName');
            if (empty($table)) {
                echo returnResponse(false, [], 'table name required.');
            }

            $this->db->from($table);
            $data = $this->db->get()->result_array();
            echo returnResponse(true, $data, 'table data found.');
        } catch (\Exception $e) {
            echo returnResponse(false, [], 'data not found.');
        }
    }

    // Save Profile
    public function saveProfile()
    {
        try {
            $id = $this->input->post('id');
            $data = $this->input->post();
            $msg = 'Great, a new profile was created successfully!';
            if (empty($id)) {
                $this->db->insert('profiles', $data);
            } else {
                $this->db->where('id', $id);
                $this->db->update('profiles', $data);
                $msg = "Great, a profile was saved successfully!";
            }
            echo returnResponse(true, [], $msg);
        } catch (\Exception $e) {
            echo returnResponse(false, [], "Umh, We can't store profile right now");
        }
    }

    // Get Profiles listing
    public function projects($id = '')
    {
        try {
            if (!empty($id)) {
                $this->db->where('id', $id);
            }
            $this->db->from('projects');
            $query = $this->db->get();
            if (!empty($id)) {
                $data = $query->row_array();
                if (count($data) <= 0) {
                    echo returnResponse(false, [], 'projects listing empty.');
                    die;
                }
            } else {
                $data = $query->result_array();
            }
            echo returnResponse(true, $data, 'projects listing');
        } catch (\Exception $e) {
            echo returnResponse(false, [], 'projects listing empty.');
        }
    }
}