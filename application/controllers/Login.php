<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Init Server OAuth Object
        $this->load->library('CIAuth');
    }

    // Load Login view
    public function index()
    {
        if ($this->session->has_userdata('isLoggedIn') && $this->session->userdata('isLoggedIn') == 1) {
            redirect('/welcome');
        }
        $this->load->view("user/login");
    }

    // Get Access Token
    public function doLogin()
    {
        try {
            // find user in db
            $query = $this->db->get_where('users', array(
                'email' => $this->input->post('client_id'),
                'password' => md5($this->input->post('client_secret')),
            ), 1);
            $userData = $query->row_array();
            if (count($userData) > 0) {
                // Handle a request for an OAuth2.0 Access Token and send the response to the client
                $getRs = $this->ciauth->server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
                $getRs = json_decode($getRs, true);
                if (isset($getRs['access_token'])) {
                    // User logged In
                    $this->session->set_userdata('isLoggedIn', 1);
                    $this->session->set_userdata('userData', $userData);
                    redirect('/welcome');
                } else {
                    // Invalid Client Credentials
                    $this->session->set_flashdata('login_status', 'Email/Password Invalid.');
                    redirect('/');
                }
            } else {
                // Invalid Client Credentials
                $this->session->set_flashdata('login_status', 'Email/Password Invalid.');
                redirect('/');
            }
        } catch (\Exception $e) {
            // Invalid Client Credentials
            $this->session->set_flashdata('login_status', 'Email/Password Invalid.');
            redirect('/');
        }
    }

    // Get User Detail and Verify Access Token
    public function resource()
    {
        try {
            // Handle a request to a resource and authenticate the access token
            if (!$this->ciauth->server->verifyResourceRequest(OAuth2\Request::createFromGlobals())) {
                echo json_encode(array('status' => false, 'data' => [], 'message' => 'user logged in failed.'));
                die;
            }

            echo json_encode(array('status' => true, 'data' => [], 'message' => 'user logged in.'));
        } catch (\Exception $e) {
            echo json_encode(array('status' => false, 'data' => [], 'message' => 'user logged in failed.'));
        }
    }

    // Auth Logout
    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/');
    }

    // Get Logged In User Detail - Session
    public function getAuthDetail()
    {
        if ($this->session->has_userdata('userData')) {
            echo returnResponse(true, $this->session->userdata('userData'), 'user data found');
            die;
        }

        echo returnResponse(false, [], 'no data found');
    }

    // Load Login view
    public function forgetPassword()
    {
        if (count($this->input->post()) > 0) {
            $this->db->where('email', $this->input->post('email'));
            $this->db->from('users');
            $uData = $this->db->get()->row_array();
            if (count($uData) > 0) {
                $clientData = array(
                    'client_secret' => $this->input->post('password')
                );
                $data = array(
                    'password' => md5($this->input->post('password'))
                );
                $this->db->where('client_id', $this->input->post('email'));
                $clientSave = $this->db->update('oauth_clients', $clientData);

                if ($clientSave) {
                    $this->db->where('id', $uData['id']);
                    $this->db->update('users', $data);
                    redirect('/');
                } else {
                    // Invalid Client Credentials
                    $this->session->set_flashdata('resetStatus', 'Please verify your email.');
                    redirect('reset/password');
                }
            } else {
                // Invalid Client Credentials
                $this->session->set_flashdata('resetStatus', 'Email not found.');
                redirect('reset/password');
            }
        }

        $this->load->view("user/reset-password");
    }

    public function rest()
    {
        $getRs = $this->ciauth->server->handleTokenRequest(OAuth2\Request::createFromGlobals())->send();
        echo $getRs;
    }
}